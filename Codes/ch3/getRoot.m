function[root] = getRoot(a,minError)
imag=false;
if (a<=0)
    a=abs(a);
    imag=true;
end
xOld=a/2;
xNew=xOld;

while (xOld>0)
    xNew= (xOld+(a./xOld))./2;
    
    error = abs((xNew-xOld)./xOld);
    xOld=xNew;
    
    if( error <= minError)
        break;
    end
end
root=xNew;
if (imag)
    root=root*j;
    imag=false;
end
end

