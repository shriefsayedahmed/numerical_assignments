plot(x,fx)
x=0:0.01:20;
fx=((x+4)./(((42-(2.*x)).^2).*(28-x)))-0.016;
plot(x,fx)
xlabel('X')
ylabel('f(x)')
x(find(fx==0))