clear all;
clc;
%% P3_6
X=[2 2 0 -3 -2 -1 0 0 2];
Y=[0 1 3 1 0 -2 0 -2 2];
siz= size(X);
for i=1:siz(2)
    [r(i),theta(i)]=getTheta_R(X(i),Y(i));
end
T = table(X',Y',r',theta', 'VariableNames',{'X    ' 'Y     ' 'r     ' 'Theta'})
%% p3_12
clear all;
clc;
vactorize();

%% p3_13
clear all;
clc;
minError =1.0E-4;

a=[2,9,64,10,-4];
[aa siz]=size(a);

for i=1:siz
 root(i)= getRoot(a(i),minError);
end
root

%% p3_14
clear all;
close all;
clc;
time = -5:50;
velocity = getVelocity(time);
figure,plot(time,velocity);
title('Relation between Velocity and Time ')
xlabel('Time')
ylabel('Velocity')

%% p3_19
%TBD
%% p3_22
clear all;
close all;
clc;
r = 1;
nt = 256;
nm = 10;
theta = 0: pi/nt :2* pi;

phasor (r,nt,nm,theta);