clc;
clear all;
close all;
%% (a) Graphical Solution
x = -10:10;
y1 = x.^3 - 6*x.^2 + 11*x - 6.1;
y2 = zeros (1,21);
figure,plot(x,y1,'-r')
hold on
plot(x,y2,'-b') ,title('Graphical Solution');
legend('y1 = x.^3 - 6*x.^2 + 11*x - 6.1','y2 = 0')

