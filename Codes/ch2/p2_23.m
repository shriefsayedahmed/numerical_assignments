ceta=0:pi/32:8*pi;
r=exp(sin(ceta))...
    -2*cos(4*ceta)...
    -sin((20-ceta)./24).^5;
polar(ceta,r,'--r')
title(' poplar plot for r versus ceta of butterfly curve')
xlablel('ceta')
ylabel('r')