function[velocity]=getVelocity(time)
[b s]=size(time);
for i=1:s
    velocity(i)=0;
    if (time(i) > 0)
        if (time(i) < 8)
            velocity(i) = 10*time(i).^2-5*time(i);
        elseif(time(i) <16)
            velocity(i) = 624-5*time(i);
        elseif(time(i) <26)
            velocity(i) = (36*time(i))+(12*((time(i)-16).^2));
        else
            velocity(i)= 2136*exp(-0.1*(time(i)-26));
        end
    end
end