T0=3000;
T=1000;
Mu0=1360;
q=1.5*(10^(-17));
ni=6.2*(10^(8));
ro=6.5*(10^(6));
NO=[0 2.5*(10^(10))];
Mu=Mu0*(T/T0)-2.42;
fn=@(N) 0.5*(N+sqrt(N^2+4*ni^2));
fro=@(N) 1/(q*fn(N)*Mu);
fro=@(N) 1/(q*fn(N)*Mu)-ro;


%% Using Bisection Method

Xl=NO(1);
Xu=NO(2);
for i=1:20
    Xr=(Xl+Xu)/2;
    if(fro(Xl)*fro(Xu)<0)
        Xu=Xr;
    else
        Xl=Xr;
    end
end
Bisection_root=Xr;

%% Using False Positive Method

Xl=NO(1);
Xu=NO(2);
for i=1:20
    Xr=Xu-((fro(Xu)*(Xl-Xu))/(fro(Xl))-(fro(Xu)));
    if(fro(Xl)*fro(Xu)<0)
        Xu=Xr;
    else
        Xl=Xr;
    end
end
False_Positive_root=Xr;
