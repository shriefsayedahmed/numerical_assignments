clc;
clear all;
close all;
t = 0:0.03125:0.25;
dt = 0.03125;
d = 0.01;
cd = 0.47;
rho_m = 2700;
rho_w = 1000;
g = 9.81;
v = zeros(1,9);
v(1) = 0;
for i = 1:8
 v(i+1) = v(i) + (((1-(rho_w/rho_m))*g)-(((6*cd)/(rho_m*pi*d^3))*v(i)^2))*dt;
end
figure, plot(t,v);
xlabel('Time') % x-axis label
ylabel('Velocity') % y-axis label