% Numerical solution
clear all;
close all;
clc;
f = @(x) -8*x(1) + x(1)^2 + 12*x(2) + 4*x(2)^2 - 2*x(1)*x(2);
[x fval] = fminsearch(f , [0 , 0]);