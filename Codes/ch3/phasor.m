function phasor(r,nt,nm,theta)

for i = 1: nm
    close all;
    figure (1), axis ([ -1 1 -1 1]) ;
    hold on;
    for j = theta (1: end )
        x = r*cos(j) ;
        y = r*sin(j) ;
        plot (x,y,'x') ;
        pause (0.01) ;
    end
end
end