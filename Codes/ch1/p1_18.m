clc;
clear all;
close all;
#a a) Solution
t = 0:0.5:5;
T = zeros(1,11);
T(1) = 37;
Ta = 10;
dt = 0.5;
k = 0.12;
for i = 1:10
 T(i+1) = T(i) + (-k*(T(i) - Ta))*dt;
end
figure, plot(t,T);
xlabel('Time') % x-axis label
ylabel('Temperature') % y-axis label
# b) Solution
Tb = zeros(1,11);
Tb(1) = 37;
Ta_n = 20:-1:10;
for i = 1:10
 Tb(i+1) = Tb(i) + (-k*(T(i) - Ta_n(i)))*dt;
end
figure, plot(t,Tb);
xlabel('Time') % x-axis label
ylabel('Temperature') % y-axis label
# c) Solution
figure ,plot(t,T,'-r');
hold
plot(t,Tb,'-b');
xlabel('Time') % x-axis label
ylabel('Temperature') % y-axis label
legend('Ta = 10 C','Ta = 20 to 10')