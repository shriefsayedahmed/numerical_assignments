function [r,theta] = getTheta_R(X,Y)
r= sqrt ( X.^2+Y.^2);

%% different signs
if (X<0 && Y<0)
    theta = atan(Y./X) -pi/2;
elseif (X<0 && Y>0)
    theta = atan(Y./X) +pi/2;
elseif (X>0 && Y<0)
    theta =atan(Y./X)+2*pi;
elseif (X>0 && Y>0)
    theta =atan(Y./X);
elseif (X<0 && Y>0)
    theta =atan(Y./X)+pi;
    
    %% @Y=0
elseif (X<0 && Y==0)
    theta =pi;
elseif (X>0 && Y==0)
    theta =0;
    %% @X=0
elseif (X==0 && Y<0)
    theta =-pi/2;
    
elseif (X==0 && Y>0)
    theta =pi/2;
    %% @X and Y =0
elseif (X==0 && Y==0)
    theta=0;
end
theta=theta*180/pi;
end