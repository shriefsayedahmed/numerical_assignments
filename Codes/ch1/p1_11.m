clc;
clear all;
close all;
t = 0:0.5:10;
yout = 1;
dt = 0.5;
y = zeros(1,21);
y(1) = 0.8;

for i = 1:20
  if y(i) > yout
    y(i+1) = y(i) + ((3*sin(t(i))^2 - (3*(y(i) - yout)^1.5))/(pi*(0.625*y(i))^2))*dt;
  else
    y(i+1) = y(i) + ((3*sin(t(i))^2)/(pi*(0.625*y(i))^2))*dt;
   end
end
figure, plot(t,y);
xlabel('Time') % x-axis label
ylabel('Depth') % y-axis label