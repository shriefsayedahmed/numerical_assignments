% Graphical solution
clear all;
close all;
clc;
x=linspace(-2,5,40);y=linspace(-3,5,40);
[X,Y] = meshgrid(x,y);
Z=-8*X + X.^2 + 12*Y + 4*Y.^2 - 2*X.*Y;     
subplot(1,2,1);
cs=contour(X,Y,Z);clabel(cs);
xlabel('x');ylabel('y');
title('(a) Contour plot');grid;
subplot(1,2,2);
cs=surfc(X,Y,Z);
zmin=floor(min(Z))
zmax=ceil(max(Z));
xlabel('x');ylabel('y');zlabel('f(x,y)');
title('(b) Mesh plot');
