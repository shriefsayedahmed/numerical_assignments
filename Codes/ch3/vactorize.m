function vactorize()
clear all;
clc;
tstart=0; tend=20; ni=8;
%% Before Vactorizing
tic
t(1)=tstart;
y(1)=12 + 6*cos(2*pi*t(1)/(tend-tstart));
for i=2:ni+1
t(i)=t(i-1)+(tend-tstart)/ni;
y(i)=10 + 5*cos(2*pi*t(i)/ (tend-tstart));
end
y
toc

%% After Vactorizing
tic
m=1:ni+1;
t2=(m-1)*(tend-tstart)/ni;
y2=10+3*(m==1)+5*cos(2*pi*t2/ (tend-tstart))
toc
end