clc;
clear all;
close all;
#Numerical solution
x=0:10000:100000;
v =zeros(1,11);
v(1) = 1500;
dx = 10000
g0 = 9.81;
R = 6.37*10^6;
for i = 1:10
 v(i+1) = v(i) + ((g0*R^2*dx)/(x(i) +(R))^2)*(1/v(i));

end
#Analytical solution
va =zeros(1,11);
va(1) = 1500;
for i = 1:10
 va(i+1) = sqrt(v(1)^2 -2*g0*R*((1/(1+(x(i+1)/R)))-1));
end
figure ,plot(x,v,'-r');
hold
plot(x,va,'-b');
xlabel('Altitude') % x-axis label
ylabel('Velocity') % y-axis label
legend('Numerical Solution','Analytical Solution')
